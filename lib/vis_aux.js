//function that receives fetches from URL GET the states array and returns JSON object
function getStates() {
    var states = decodeURIComponent(window.location.search.substring(window.location.search.indexOf('states=') + 7));

    if (states.indexOf('&') >= 0) {
        states = states.substring(0, states.indexOf('&'));
    }
    return states;
}

//function that receives JSON object, filters object to the rows that contain the state and executes the callback function once finished.
function fetchFilteredDatasetByState(datasetPath,filterStatesArray,callbackFunction){
    var result, err;
    filterStatesArray = filterStatesArray.toLowerCase();
    d3.json(datasetPath, function(err, d){
        if(err){
            console.log(err);
        } else{ 
            result = d.data.filter(function (i){ 
                    return (filterStatesArray.indexOf(i.State.toLowerCase()) > -1);
                });
        }
        if(callbackFunction){
            callbackFunction(err,result);
        }
    });
}
function fetchDataset(datasetPath,callbackFunction){
    d3.json(datasetPath, function(err, d){
        if(err){
            console.log(err);
        }
        if(callbackFunction){
            callbackFunction(err,d);
        }
    });
}