importScripts: "../lib/d3.v3.min.js";
//cor #1B0047
var config = { "unselectedColor": "#c0c0c0", "selectedColor": "#bb1744", "stateDataColumn": "state_or_territory", "valueDataColumn": "population_estimate_for_july_1_2013_number" }

var WIDTH = "100%", HEIGHT = "50%";

var COLOR_COUNTS = 9;

var SCALE = 0.6;
var valueById = d3.map();
var stateList;

//ON LOAD CODE
Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};
//Function to convert hex format to a rgb color
function rgb2hex(rgb) {
  var hexDigits = new Array
    ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");

  function hex(x) {
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
  }
  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}

var unselectedRGB = hexToRgb(config.unselectedColor);
var selectedRGB = hexToRgb(config.selectedColor);

//GLOBAL VARIABLES
var stateSelectionList;
var clearAllStates = null;

//ON LOAD CODE
Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

function flagMove() {
  var html = "";

  html += "<div class=\"tooltip_kv\">";
  html += "<span class=\"tooltip_key\">";
  html += "All States";
  html += "</span>";
  html += "</div>";

  $("#tooltip-container").html(html);
  $(this).attr("fill-opacity", "0.8");
  $("#tooltip-container").show();

  var coordinates = d3.mouse(this);

  var map_width = $('.states-choropleth')[0].getBoundingClientRect().width;

  if (d3.event.layerX < map_width / 2) {
    d3.select("#tooltip-container")
      .style("top", (d3.event.layerY + 15) + "px")
      .style("left", (d3.event.layerX + 15) + "px");
  } else {
    var tooltip_width = $("#tooltip-container").width();
    d3.select("#tooltip-container")
      .style("top", (d3.event.layerY + 15) + "px")
      .style("left", (d3.event.layerX - tooltip_width - 30) + "px");
  }
}

function flagMoveOut() {
  $(this).attr("fill-opacity", "1.0");
  $("#tooltip-container").hide();
}

function clickFlag() {
  var flag = document.getElementById('flag');
  var ar = getStateSelectionList();
  ar = [];
  setStateSelectionList(ar);
  //clean state Array
  stateSelectionList.value.length = 0;

  if (flag.className === "unsel-flag") {
    //ar.push("all states");
    flag.className = "sel-flag";
    // ar.style.color="rgb(187, 23, 68)";
    stateList.forEach(function (element) {
      ar.push(element.name.toLowerCase());
    }, this);
    d3.selectAll("path").style("fill", function (d) {
      if (valueById.get(d.id)) {
        var i = 0//quantize(valueById.get(d.id));
        var color = selectedRGB;
        return "rgb(" + color.r + "," + color.g +
          "," + color.b + ")";
      } else {
        return "";
      }
    });
  }
  else {
    ar.remove("all states");
    flag.className = "unsel-flag";
    //clean state Array
    stateSelectionList.value.length = 0;

    d3.selectAll("path").style("fill", function (d) {
      if (valueById.get(d.id)) {
        var i = 0//quantize(valueById.get(d.id));
        var color = unselectedRGB;
        return "rgb(" + color.r + "," + color.g +
          "," + color.b + ")";
      } else {
        return "";
      }
    });
    
    //clean all colors
    cleanFlag();

  }
  setStateSelectionList(ar);
}
function cleanFlag() {
  var flag = document.getElementById('flag');
  var ar = getStateSelectionList();

  ar.remove("all states");
  flag.className = "unsel-flag";

  setStateSelectionList(ar);
}

function getStateSelectionList() {
  stateSelectionList = document.getElementById('selectedStatesList');
  return JSON.parse(stateSelectionList.value);
}

function setStateSelectionList(array) {
  stateSelectionList = document.getElementById('selectedStatesList');
  return stateSelectionList.value = JSON.stringify(array);
}

d3.csv("data/population.csv", function (err, data) {

  clearAllStates = function () {
    setStateSelectionList([]);
    //clean state Array
    stateSelectionList.value.length = 0;
    //clean all colors
    cleanFlag();

    d3.selectAll("path").style("fill", function (d) {
      if (valueById.get(d.id)) {
        var i = 0//quantize(valueById.get(d.id));
        var color = unselectedRGB;
        return "rgb(" + color.r + "," + color.g +
          "," + color.b + ")";
      } else {
        return "";
      }
    });
  }



  var MAP_STATE = config.stateDataColumn;
  var MAP_VALUE = config.valueDataColumn;

  var width = WIDTH,
    height = HEIGHT;



  var quantize = d3.scale.quantize()
    .domain([0, 1.0])
    .range(d3.range(COLOR_COUNTS).map(function (i) { return i }));

  var path = d3.geo.path();

  var svg = d3.select("#the_chart").append("svg")
    .attr("width", width)
    .attr("height", height);

  d3.tsv("data/us-state-names.tsv", function (error, names) {

    name_id_map = {};
    id_name_map = {};
    stateList = names;
    for (var i = 0; i < names.length; i++) {
      name_id_map[names[i].name] = names[i].id;
      id_name_map[names[i].id] = names[i].name;
    }

    data.forEach(function (d) {
      var id = name_id_map[d[MAP_STATE]];
      valueById.set(id, +d[MAP_VALUE]);
    });

    quantize.domain([d3.min(data, function (d) { return +d[MAP_VALUE] }),
      d3.max(data, function (d) { return +d[MAP_VALUE] })]);

    d3.json("data/us.json", function (error, us) {
      svg.append("g")
        .attr("class", "states-choropleth")
        .selectAll("path")
        .data(topojson.feature(us, us.objects.states).features)
        .enter().append("path")
        .attr("transform", "scale(" + SCALE + ")")
        .attr('cursor', "pointer")
        .style("fill", function (d) {
          if (valueById.get(d.id)) {
            var i = 0//quantize(valueById.get(d.id));
            var color = unselectedRGB;
            return "rgb(" + color.r + "," + color.g +
              "," + color.b + ")";
          } else {
            return "";
          }
        })
        .attr("d", path)
        .on("mousemove", function (d) {
          var html = "";

          html += "<div class=\"tooltip_kv\">";
          html += "<span class=\"tooltip_key\">";
          html += id_name_map[d.id];
          html += "</span>";
          html += "</div>";

          $("#tooltip-container").html(html);
          $(this).attr("fill-opacity", "0.8");
          $("#tooltip-container").show();

          var coordinates = d3.mouse(this);

          var map_width = $('.states-choropleth')[0].getBoundingClientRect().width;

          if (d3.event.layerX < map_width / 2) {
            d3.select("#tooltip-container")
              .style("top", (d3.event.layerY + 15) + "px")
              .style("left", (d3.event.layerX + 15) + "px");
          } else {
            var tooltip_width = $("#tooltip-container").width();
            d3.select("#tooltip-container")
              .style("top", (d3.event.layerY + 15) + "px")
              .style("left", (d3.event.layerX - tooltip_width - 30) + "px");
          }
        })
        .on("mouseout", function () {
          $(this).attr("fill-opacity", "1.0");
          $("#tooltip-container").hide();
        })
        .on("click", function (d) {
          var colorState = $(this).css("fill")
          colorState = rgb2hex(colorState.toString());
          var ar = getStateSelectionList();

          if (colorState === config.selectedColor) {
            colorState = config.unselectedColor;
            ar.remove(id_name_map[d.id].toLowerCase());
            setStateSelectionList(ar);
            cleanFlag()
          } else {
            colorState = config.selectedColor;
            ar.push(id_name_map[d.id].toLowerCase());
            setStateSelectionList(ar);
            if (ar.length >= 60) {
              clickFlag();
            }
          }


          d3.select(this).style("fill", colorState);

        })
      svg.append("path")
        .datum(topojson.mesh(us, us.objects.states, function (a, b) { return a !== b; }))
        .attr("class", "states")
        .attr("transform", "scale(" + SCALE + ")")
        .attr("d", path);
    });

  });
});
