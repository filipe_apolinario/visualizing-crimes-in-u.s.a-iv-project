var color;
var chartBuilder = function (jsonObj) {
  var dataset = jsonObj;
  var showGender = ["0-20", "20-40", "40-60", "60-80", "80-100"];
  var isSVGCreated = false;
  //ON LOAD CODE
  Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
      what = a[--L];
      while ((ax = this.indexOf(what)) !== -1) {
        this.splice(ax, 1);
      }
    }
    return this;
  };
  function colorPicker(percentage) {
    if (percentage <= 20 && showGender.indexOf("0-20") > -1) {
      return "#FF00AA"
    }
    else if (percentage > 20 && percentage <= 40 && showGender.indexOf("20-40") > -1) {
      return "#FF12DB";
    }
    else if (percentage > 40 && percentage <= 60 && showGender.indexOf("40-60") > -1) {
      return "#D850FF"
    }
    else if (percentage > 60 && percentage <= 80 && showGender.indexOf("60-80") > -1) {
      return "#8B73FF";
    }
    else if (percentage > 80 && showGender.indexOf("80-100") > -1)
      return "#008CFF";
    else return "#FFFFFF";
  }

  function colorRegionMapper(interval) {
    return colorPicker(interval.split("-")[1])
  }
  var margin = { top: 20, right: 20, bottom: 30, left: 80 },
    width = 900 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

  var x = d3.scale.linear()
    .range([0, width]);

  var y = d3.scale.linear()
    .range([height, 0]);

    
  /*var*/ color = d3.scale.ordinal().domain([0, 100]).range(["#FF00AA", "#FF12DB", "#D850FF", "#8B73FF", "#008CFF"]);
  /*console.log(color(60));
  console.log(color(65));
  console.log(color(70));
  console.log(color(75));
  console.log(color.domain())*/
  var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    .tickFormat('')
    .tickSize(0)

  var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

  var zoom = d3.behavior.zoom()
    .scaleExtent([1, 10])
    .on("zoom", zoomed);

  function zoomed() {
    container.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
  }

  function refreshButtons() {
    if (showGender.length > 0) {
      document.getElementById('cleanYears').disabled = false;
      if (showGender.length >= 5) {
        document.getElementById('selectAllYears').disabled = true;
      }
      else document.getElementById('selectAllYears').disabled = false;
    }
    else {
      document.getElementById('selectAllYears').disabled = false;
      document.getElementById('cleanYears').disabled = true;
    }
  }
  function initializeChart() {
    refreshButtons();
    if (isSVGCreated) {
      d3.select("svg").remove();
    }
    document.getElementById('cleanYears');
    isSVGCreated = true;
    var svg = d3.select("#the_chart").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .call(zoom);

    dataset.forEach(function (d) {
      d.GenderPercentage = parseFloat(d.GenderPercentage)
    });
    //d3.tsv("data.tsv", function (error, data) {
    //if (error) throw error;

    x.domain([0, width]).nice();

    y.domain([0, d3.max(dataset, function (d) {
      return d.CrimeRatePop100000;
    })]).nice();

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .append("text")
      .attr("class", "label")
      .attr("x", width)
      .attr("y", -6)
      .style("text-anchor", "end")
    //.text("Sepal Width (cm)");

    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("class", "label")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("crimes/population")

    svg.selectAll(".dot")
      .data(dataset)
      .enter().append("circle")
      .attr("class", "dot")
      .attr("r", 3.5)
      .attr("cx", function (d) { return x(Math.floor((Math.random() * (width - width * .1)) + 1)); })
      .attr("cy", function (d) { return y(d.CrimeRatePop100000); })
      .style("fill", function (d) { return colorPicker(d.GenderPercentage) })
      .on("mousemove", function (d) {
        var html = "";

        html += "<div class=\"tooltip_kv\">";
        html += "<span class=\"tooltip_key\">";
        html += "State: " + d.State + "<br>";
        html += "Year: " + d.Year + "<br>";
        html += "Crimes: " + d.CrimeRatePop100000 + "<br>";
        html += "Gender: " + d.GenderPercentage;
        html += "</span>";
        html += "</div>";

        $("#tooltip-container").html(html);
        $(this).attr("fill-opacity", "0.8");
        $("#tooltip-container").show();
                
        //if (d3.event.layerX < width / 2) {
        d3.select("#tooltip-container")
          .style("top", (d3.event.layerY + 15) + "px")
          .style("left", (d3.event.layerX + 15) + "px");
        /*} else {
            var tooltip_width = $("#tooltip-container").width();
            d3.select("#tooltip-container")
                .style("top", (d3.event.layerY + 15) + "px")
                .style("left", (d3.event.layerX - tooltip_width - 30) + "px");
        }*/
      })
      .on("mouseout", function () {
        $(this).attr("fill-opacity", "1.0");
        $("#tooltip-container").hide();
      });

    var legend = svg.append("g")
      .attr("class", "legends")

    var label = "Percentage of Male officers:"
    legend.append("text")
      .attr("class", "label")
      .text(label)
      .attr("x", width - label.length * 4)
      .attr("y", 0)

    legend = legend.selectAll(".legend")
      .data(["0-20", "20-40", "40-60", "60-80", "80-100"])
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function (d, i) { return "translate(0," + (i * 20 +4) +  ")"; });
    
    //legend.append("text")
    //  .text("Percentage of Male officers in the force:");  
    legend.append("rect")
      .attr("x", width - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", colorRegionMapper)
      .style("stroke", "black")
      .style("stroke-width", 1)
      .style("cursor", "pointer")
      .on("click", function (d) {
        if (showGender.indexOf(d) <= -1) {
          showGender.push(d)
        }
        else showGender.remove(d);
        svg.selectAll(".dot").style("fill", function (d) { return colorPicker(d.GenderPercentage) })
        svg.selectAll(".dot").style("class", function (d) { if (showGender.indexOf(d) < 0) return ".dot_unsel"; else return ".dot"; })
        svg.selectAll("rect").style("fill", colorRegionMapper)
        //alert("Info " + d)
        refreshButtons();
      })

    legend.append("text")
      .attr("x", width - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function (d) {
        return d;
      });
    document.getElementById('cleanYears').onclick = function () {
      showGender = [];
      svg.selectAll(".dot").style("fill", function (d) { return colorPicker(d.GenderPercentage) })
      svg.selectAll(".dot").style("class", function (d) { if (showGender.indexOf(d) < 0) return ".dot_unsel"; else return ".dot"; })
      svg.selectAll("rect").style("fill", colorRegionMapper)
      refreshButtons();
    }
    document.getElementById('selectAllYears').onclick = function () {
      showGender = ["0-20", "20-40", "40-60", "60-80", "80-100"];
      svg.selectAll(".dot").style("fill", function (d) { return colorPicker(d.GenderPercentage) })
      svg.selectAll(".dot").style("class", function (d) { if (showGender.indexOf(d) < 0) return ".dot_unsel"; else return ".dot"; })
      svg.selectAll("rect").style("fill", colorRegionMapper)
      refreshButtons();
    }
  }
  return {
    'initializeChart': initializeChart,
  }

}
