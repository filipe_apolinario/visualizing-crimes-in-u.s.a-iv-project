var trackingStates  = [];
var config = { "unselectedColor": "#c0c0c0", "selectedColor": "#bb1744", "stateDataColumn": "state_or_territory", "valueDataColumn": "population_estimate_for_july_1_2013_number" }
//ON LOAD CODE
Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

function rgb2hex(rgb) {
    var hexDigits = new Array
      ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");

    function hex(x) {
      return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
  }

  function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }

  var unselectedRGB = hexToRgb(config.unselectedColor);
  var selectedRGB = hexToRgb(config.selectedColor);

var chartBuilder = function (jsonObj) {
  var dataset = jsonObj,
    filteredDataset,
    year = 2010;

  function initializeChart() {
    var currentDotRadius = 4;
    var margin = { top: 5, right: 200, bottom: 40, left: 40 },
      width = 950 - margin.left - margin.right,
      height = 550 - margin.top - margin.bottom;
    
    // setup x 
    var xScale = d3.scale.linear().range([0, width]), // value -> display
      xMap = function (d) { return xScale(xValue(d)); }, // data -> display
      xAxis = d3.svg.axis().scale(xScale).orient("bottom");

    // setup y
    var yScale = d3.scale.linear().range([height, 0]), // value -> display
      yMap = function (d) { return yScale(yValue(d)); }, // data -> display
      yAxis = d3.svg.axis().scale(yScale).orient("left");

    // add the graph canvas to the body of the webpage
    var svg = d3.select("body").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // add the tooltip area to the webpage
    var tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .style("opacity", 0);
      	
    // x-axis
    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height / 2 + ")")
      .call(xAxis)
      .append("text")
      .attr("class", "label")
      .attr("x", width)
      .attr("y", -6)
      .style("text-anchor", "end")
      .text(xaxisText);

    // y-axis
    svg.append("g")
      .attr("class", "y axis")
      .attr("transform", "translate(" + width / 2 + ",0)")
      .call(yAxis)
      .append("text")
      .attr("class", "label")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text(yaxisText);

    // draw the slider
    d3.select('#slider')
      .style("cursor","pointer")
      .call(d3.slider()
        .axis(true)
        .min(2010)
        .max(2014)
        .step(1)
        .value(2010).on("slide", function (evt, value) {
          updateData(value);
          d3.select('#slidertext').text(year);
        }));
    d3.select('handle-one').style
    // don't want dots overlapping axis, so add in buffer to dataset domain
    xScale.domain([d3.min(dataset, xValue) - 1, d3.max(dataset, xValue) + 1]);
    yScale.domain([d3.min(dataset, yValue) - 1, d3.max(dataset, yValue) + 1]);

    filteredDataset = dataset.filter(function (d) { return d.Year == year; });
      
    // draw dots
    var dots = svg.selectAll(".dot").data(filteredDataset);
    dots.enter().append("circle")
      .attr("class", "dot")
      .attr('cursor',"pointer")
      .attr("r", 4)
      .attr("cx", xMap)
      .attr("cy", yMap)
      .style("fill", config.unselectedColor)
      .style("z-index", "10")
      .on("mouseover", function (d) {
        d3.select(this).transition()
          .duration(200)
          .attr("r", 6)
          .style("fill", config.selectedColor);
        tooltip.transition()
          .duration(200)
          .style("opacity", .9);
        tooltip.html(d.State + "<br/>" + d.Year + "<br/>(" + xValue(d) + "," + yValue(d) + ")")
          .style("left", (d3.event.pageX + 10) + "px")
          .style("top", (d3.event.pageY - 28) + "px");
      })
      .on("mouseout", function (d) {
        var c;
        if(trackingStates.indexOf(d.State) < 0){
          c = config.unselectedColor;
        }
        else c = config.selectedColor;
        
        d3.select(this).transition()
          .duration(200)
          .attr("r", 4)
          .style("fill", c);
        tooltip.transition()
          .duration(500)
          .style("opacity", 0);
      })
      .on("click", function (d) {
        var colorState;
          if(trackingStates.indexOf(d.State) < 0){
            colorState = config.selectedColor;  
            trackingStates.push(d.State);
          } else {
            colorState = config.unselectedColor;
            trackingStates.remove(d.State)
          }
          currentDotRadius = currentDotRadius == 4 ? 6 : 4;
        
          console.log("COLOR",colorState)
           d3.select(this).transition()
          .duration(200)
          .attr("r", currentDotRadius)
          .style("fill", colorState);
      })

    // Draw the axis
    svg.select(".x.axis").call(xAxis);
    svg.select(".y.axis").call(yAxis);

    // Calculate linear regression
    var xval = filteredDataset.map(function (d) { return parseFloat(xValue(d)); });
    var yval = filteredDataset.map(function (d) { return parseFloat(yValue(d)); });
    var lr = linearRegression(yval, xval);

    // apply the reults of the least squares regression
    
    var x1 = d3.min(filteredDataset, function (d) { return xValue(d); });
    var x2 = d3.max(filteredDataset, function (d) { return xValue(d); });
    var y1 = lr.intercept;
    var y2 = x2 * lr.slope + lr.intercept;
    var trendData = [[x1, y1, x2, y2]];

    // draw trendline
    svg.selectAll(".trendline")
      .data(trendData)
      .enter().append("line")
      .attr("class", "trendline")
      .attr("x1", function (d) { return xScale(d[0]); })
      .attr("y1", function (d) { return yScale(d[1]); })
      .attr("x2", function (d) { return xScale(d[2]); })
      .attr("y2", function (d) { return yScale(d[3]); })     
    // display equation on the chart
      .on("mouseover", function (d) {
        tooltip.transition()
          .duration(200)
          .style("opacity", .9);
        tooltip.html("eq: " + d3.round(lr.slope, 2) + "x + " + d3.round(lr.intercept, 2) + "<br/>" +
          "r-sq: " + d3.round(lr.r2, 2))
          .style("left", (d3.event.pageX + 5) + "px")
          .style("top", (d3.event.pageY - 28) + "px");
      })
      .on("mouseout", function (d) {
        tooltip.transition()
          .duration(500)
          .style("opacity", 0);
      });
    svg.selectAll(".trendline2")
      .data(trendData)
      .enter().append("line")
      .attr("class", "trendline2")
      .style("stroke-dasharray", ("3, 3"))
      .attr("x1", function (d) { return xScale(d[0]); })
      .attr("y1", function (d) { return yScale(d[1]); })
      .attr("x2", function (d) { return xScale(d[2]); })
      .attr("y2", function (d) { return yScale(d[3]); });
  
    // draw the data 
    function draw() {
      // filter the data before drawing
      filteredDataset = dataset.filter(function (d) { return d.Year == year; });
	  	
      // Update the dots
      var dots = svg.selectAll(".dot").data(filteredDataset);
      dots.transition()
        .duration(1500)
        .attr("cx", xMap)
        .attr("cy", yMap);

      // Update the axis  
      svg.select(".x.axis").call(xAxis);
      svg.select(".y.axis").call(yAxis);

      dots.exit().remove();
    }

    function updateData(x) {
      year = x;
      draw();
    }

    function linearRegression(y, x) {

      var lr = {};
      var n = y.length;
      var sum_x = 0;
      var sum_y = 0;
      var sum_xy = 0;
      var sum_xx = 0;
      var sum_yy = 0;

      for (var i = 0; i < y.length; i++) {

        sum_x += x[i];
        sum_y += y[i];
        sum_xy += (x[i] * y[i]);
        sum_xx += (x[i] * x[i]);
        sum_yy += (y[i] * y[i]);
      }

      lr['slope'] = (n * sum_xy - sum_x * sum_y) / (n * sum_xx - sum_x * sum_x);
      lr['intercept'] = (sum_y - lr.slope * sum_x) / n;
      lr['r2'] = Math.pow((n * sum_xy - sum_x * sum_y) / Math.sqrt((n * sum_xx - sum_x * sum_x) * (n * sum_yy - sum_y * sum_y)), 2);

      return lr;
    }

  }

  return {
    'initializeChart': initializeChart,

  }
}