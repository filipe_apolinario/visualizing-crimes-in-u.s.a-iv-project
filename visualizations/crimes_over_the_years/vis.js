//ON LOAD CODE
Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

function refreshButtons() {
    if (years.length > 0) {
      document.getElementById('cleanYears').disabled = false;
      if (years.length >= 6) {
        document.getElementById('selectAllYears').disabled = true;
      }
      else document.getElementById('selectAllYears').disabled = false;
    }
    else {
      document.getElementById('selectAllYears').disabled = false;
      document.getElementById('cleanYears').disabled = true;
    }
  }

var BarChart = function (jsonObj,updateFunc) {
    refreshButtons();
    var dataset = jsonObj;
    var func =updateFunc;
    var units = 100000;

    function calculateRowTotal(row) {
        return row.MotorvehicletheftNumber +
            //row.ViolentcrimeRateper, 
            row.RapelegacydefinitionNumber +
            row.PropertycrimeNumber +
            //row.RapereviseddefinitionRateper,
            row.MurderandnonnegligentmanslaughterNumber +
            //row.RapelegacydefinitionRateper,
            //row.LarcenytheftRateper,
            row.ViolentcrimeNumber +
            row.BurglaryNumber +
            row.LarcenytheftNumber +
            //row.MurderandnonnegligentmanslaughterRateper,
            row.AggravatedassaultNumber +
            //row.MotorvehicletheftRateper,
            //row.Year,
            row.RobberyNumber + 
            //row.State,
            //row.Population,
            //row.BurglaryRateper,
            //row.RobberyRateper,
            row.RapereviseddefinitionNumber
        //row.AggravatedassaultRateper,
        //row.Propertycrimerateper
            
    }

    function dashboard(id, fData){
        var barColor = 'steelblue';
        function segColor(c){ return {low:"#807dba", mid:"#e08214",high:"#41ab5d"}[c]; }
        
        // compute total for each state.
        fData.forEach(function(d){d.total=calculateRowTotal(d);});
        var filteredData = [];
        fData.forEach(function(d){
            var found = false;
            for (i = 0; i < filteredData.length ; i++){
                if(filteredData[i].Year === d.Year){
                        filteredData[i].total += d.total;
                        found = true;
                }
            }
            if(!found){
                var o = {
                    Year:d.Year,
                    total:d.total
                }
                filteredData.push(o);
            } 
        })
        
        function compare(a,b) {
            if (a.Year < b.Year)
                return -1;
            else if (a.Year > b.Year)
                return 1;
            else return 0;
        }

        filteredData.sort(compare);
        // function to handle histogram.
        function histoGram(fD){
            var hG={},    hGDim = {t: 60, r: 0, b: 30, l: 0};
            hGDim.w = 500 - hGDim.l - hGDim.r, 
            hGDim.h = 300 - hGDim.t - hGDim.b;
                
            //create svg for histogram.
            var hGsvg = d3.select(id).append("svg")
                .attr("width", hGDim.w + hGDim.l + hGDim.r)
                .attr("height", hGDim.h + hGDim.t + hGDim.b).append("g")
                .attr("transform", "translate(" + hGDim.l + "," + hGDim.t + ")");
    
            // create function for x-axis mapping.
            var x = d3.scale.ordinal().rangeRoundBands([0, hGDim.w], 0.1)
                    .domain(fD.map(function(d) {
                        return d.Year; 
                        }));
    
            // Add x-axis to the histogram svg.
            hGsvg.append("g").attr("class", "x axis")
                .attr("transform", "translate(0," + hGDim.h + ")")
                .call(d3.svg.axis().scale(x).orient("bottom"));
    
            // Create function for y-axis map.
            var y = d3.scale.linear().range([hGDim.h, 0])
                    .domain([0, d3.max(fD, function(d) { 
                        return d.total; })]);
    
            // Create bars for histogram to contain rectangles and freq labels.
            var bars = hGsvg.selectAll(".bar").data(fD).enter()
                    .append("g").attr("class", "bar");
            
            //create the rectangles.
            bars.append("rect")
                .attr("x", function(d) { return x(d.Year); })
                .attr("y", function(d) { 
                    return y(d.total); 
                    })
                .attr("width", x.rangeBand())
                .attr("height", function(d) { return hGDim.h - y(d.total); })
                .attr('fill',barColor)
                .attr('cursor',"pointer")
                .on("mouseover",mouseover)// mouseover is defined below.
                .on("mouseout",mouseout)// mouseout is defined below.
                .on("click",function(d){
                    var i = years.indexOf(d.Year);
                    if(i <= -1) {
                        years.push(d.Year);
                        func(dataset);
                        d3.select(this).style("fill", "#bb1744");
                    }
                    else {
                        years.splice(i,1);
                        func(dataset);
                        d3.select(this).style("fill", barColor);
                    }
                refreshButtons();
                });
                
                document.getElementById('cleanYears').onclick = function (){
                    d3.selectAll("rect").style("fill", function (d) {
                        console.log("INFO",d.Year)
                        if(years.indexOf(d.Year) > -1)
                            years.remove(d.Year);
                        return barColor;
                    });
                    years = []
                    func(dataset);
                    refreshButtons();
                }
                document.getElementById('selectAllYears').onclick = function (){
                    d3.selectAll("rect").style("fill", function (d) {
                        console.log("INFO",d.Year)
                        if(years.indexOf(d.Year) < 0)
                            years.push(d.Year);
                        return "#bb1744";
                    });
                    func(dataset);
                    refreshButtons();
                }
            //Create the frequency labels above the rectangles.
            bars.append("text").text(function(d){ return d.total})
                .attr("x", function(d) { return x(d.Year)+x.rangeBand()/2; })
                .attr("y", function(d) { return y(d.total)-5; })
                .attr("text-anchor", "middle")
                .attr("font-family", "Helvetica Neue");
            
            function mouseover(d){  // utility function to be called on mouseover.
                // filter for selected state.
                
                var st = fData.filter(function(s){ return s.State == d[0];})[0],
                    nD = d3.keys(st.total).map(function(s){ return {type:s, freq:st.total[s]};});
                
                // call update functions of pie-chart and legend.    
                pC.update(nD);
                leg.update(nD);
            }
            
            function mouseout(d){    // utility function to be called on mouseout.
                // reset the pie-chart and legend.
                pC.update(tF);
                leg.update(tF);
            }
            
            // create function to update the bars. This will be used by pie-chart.
            hG.update = function(nD, color){
                // update the domain of the y-axis map to reflect change in frequencies.
                y.domain([0, d3.max(nD, function(d) { return d[1]; })]);
                
                // Attach the new data to the bars.
                var bars = hGsvg.selectAll(".bar").data(nD);
                
                // transition the height and color of rectangles.
                bars.select("rect").transition().duration(500)
                    .attr("y", function(d) {return y(d[1]); })
                    .attr("height", function(d) { return hGDim.h - y(d[1]); })
                    .attr("fill", color);
    
                // transition the frequency labels location and change value.
                bars.select("text").transition().duration(500)
                    .text(function(d){ return d3.format(",")(d[1])})
                    .attr("y", function(d) {return y(d[1])-5; });            
            }        
            return hG;
        }
        var hG = histoGram(filteredData); // create the histogram.
    }
    dashboard('#the-chart',dataset);
    func(dataset);
}