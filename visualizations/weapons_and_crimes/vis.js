function convertToMatrix(dataset,mmap){
    var a = [];
    for (var i = 0; i <= 6;i++){
        a[i]=new Array(7);
        for(var j=0;j<=6;j++){
            a[i][j]=0;
        }   
    }
    
      dataset.forEach(function(d){
          var i;
          if(d.type=="robbery"){
              i = 0;
          }
          else if(d.type=="murder"){
              i = 1;
          }
          else if(d.type=="aggravated assault"){
              i = 2;
          }
          else return;
          
          a[i][3] += d.Personal_weapons;
            a[3][i] += d.Personal_weapons;
            a[i][4] += d.Other_weapons;
            a[4][i] += d.Other_weapons;
            a[i][5] += d.Firearms;
            a[5][i] += d.Firearms;
            a[i][6] += d['Knives_or_cutting instruments'];
            a[6][i] += d['Knives_or_cutting instruments'];
      })
      return a;
}
function draw(dataset) {
    queue()
        .defer(d3.json, 'data-matrix.json')
        .defer(d3.json, 'data-map.json')
        .await(function(err, matrix, mmap) { 
          if (err) console.log(err);
          var dataMatrix = convertToMatrix(dataset,mmap);
          drawChords(dataMatrix, mmap);
        });
      //*******************************************************************
      //  DRAW THE CHORD DIAGRAM
      //*******************************************************************
      function drawChords (matrix, mmap) {
        var w = 750, h = 608, r1 = h / 2, r0 = r1 - 150;

        var chord = d3.layout.chord()
            .padding(.02)
            .sortSubgroups(d3.descending)

        var arc = d3.svg.arc()
            .innerRadius(r0)
            .outerRadius(r0 + 25);

        var svg = d3.select("body").append("svg:svg")
            .attr("width", w)
            .attr("height", h)
          .append("svg:g")
            .attr("id", "circle")
            .attr("transform", "translate(" + w / 2 + "," + h / 2 + ")");

            svg.append("circle")
                .attr("r", r0 + 20);

        var rdr = chordRdr(matrix, mmap);
        chord.matrix(matrix);

        var g = svg.selectAll("g.group")
            .data(chord.groups())
          .enter().append("svg:g")
            .attr("class", "group")
            .on("mouseover", mouseover)
            .on("mouseout", mouseout);

        g.append("svg:path")
            .style("stroke", "black")
            .style("fill", function(d) { return rdr(d).gdata == "weapon" ? "#32B20D": "#8C00B2"; })
            .attr("d", arc);

        g.append("svg:text")
            .each(function(d) { d.angle = (d.startAngle + d.endAngle) / 2; })
            .attr("dy", ".35em")
            .style("font-family", "helvetica, arial, sans-serif")
            .style("font-size", "18px")
            .attr("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
            .attr("transform", function(d) {
              return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
                  + "translate(" + (r0 + 36) + ")"
                  + "rotate(" + (d.angle * 180 / Math.PI - 90)*-1 + ")";
                  + (d.angle > Math.PI ? "rotate(180)" : "")
            })
            .text(function(d) { return rdr(d).gname; });

          var chordPaths = svg.selectAll("path.chord")
                .data(chord.chords())
              .enter().append("svg:path")
                .attr("class", "chord")
                .style("stroke", "black")
                .style("fill", function(d) { return rdr(d).tname == "bla" ? "#32B20D": "#D9D1CC"; })
                .attr("d", d3.svg.chord().radius(r0))
                .on("mouseover", function (d) {
                  d3.selectAll("path.chord")
                  .style("fill", function (b) {
                      return b === d ? "#273A3D": "#D9D1CC";});
                      d3.select("#tooltip")
                    .style("visibility", "visible")
                    .html(chordTip(rdr(d)))
                    .style("top", function () { return (d3.event.pageY - 170)+"px"})
                    .style("left", function () { return (d3.event.pageX - 100)+"px";})
                })
                .on("mouseout", function (d) { d3.select("#tooltip").style("visibility", "hidden") });

          function chordTip (d) {
            var q = d3.format(",f")
            return "Chord Info:<br/>"
              + d.tname + " ↔ " + d.sname
              + ": " + q(d.tvalue) + "<br/>"
              + "<br/>";
          }

          function groupTip (d) {
            var  q = d3.format(",f")
            return "Group Info:<br/>"
                + d.gname + " : " + q(d.gvalue) + "<br/>"
          }

          function mouseover(d, i) {
            d3.select("#tooltip")
              .style("visibility", "visible")
              .html(groupTip(rdr(d)))
              .style("top", function () { return (d3.event.pageY - 80)+"px"})
              .style("left", function () { return (d3.event.pageX - 130)+"px";});

            chordPaths
              .style("fill", function(b) { 
                return rdr(d).gdata == "weapon" ? "#32B20D": "#8C00B2"; })

            chordPaths.classed("fade", function(p) {
              return p.source.index != i
                  && p.target.index != i;
            });
          }

          function mouseout(d, i) {

            d3.select("#tooltip").style("visibility", "hidden");
            chordPaths.style("fill", "#D9D1CC");

          }
      }
      
}